using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] private EnemySpawner _spawner;

    private void Awake()
    {
        _spawner.StartWork();
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            _spawner.KillEnemy();
        }
    }
}

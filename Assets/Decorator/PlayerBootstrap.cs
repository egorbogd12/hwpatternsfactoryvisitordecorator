using UnityEngine;

public class PlayerBootstrap : MonoBehaviour
{
    [SerializeField] private Player _player;

    private void Awake()
    {
        // _player.Initialize(new AngelHealth(new ElfHealth(new Health(50), 2), 4, 4, this));   
      // _player.Initialize(new ArmorHealth( new Health(100),5));
      _player.Initialize(new AngelHealth(new Health(100),3,3,this));
      Debug.Log("hp 101");
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
            _player.TakeDamage(5);

        if (Input.GetKeyUp(KeyCode.F))
            _player.Heal(5);


        if (Input.GetKeyUp(KeyCode.A))
        {
            _player.Initialize(new ArmorHealth( new Health(_player.GetCurrentHP()),4));
        }
        
        if (Input.GetKeyUp(KeyCode.D))
        {
            _player.Initialize(new Health(_player.GetCurrentHP()));
        }

    }
}

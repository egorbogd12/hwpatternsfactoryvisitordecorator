using System;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public event Action<Coin> OnDeleted;
    private int _valueToAdd;
    
    protected void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out ICoinPicker coinPicker))
        {
            coinPicker.Add(_valueToAdd);
            OnDeleted?.Invoke(this);
            Destroy(gameObject);
        }
    }
    
    public virtual void Initialize(int valueToAdd)
    {
        _valueToAdd = valueToAdd;
    }

    public void MoveToPosition(Vector3 position) => transform.position = position;
}
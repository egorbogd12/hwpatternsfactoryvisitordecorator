﻿using System;
using UnityEngine;

[Serializable]
public class CoinConfig
{
    [SerializeField] private Coin _coinPrefab;
    [SerializeField, Range(0, 50)] private int _valueToAdd;

    public int ValueToAdd => _valueToAdd;

    public Coin CoinPrefab => _coinPrefab;
}
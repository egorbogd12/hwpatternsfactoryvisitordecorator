using UnityEngine;

public class BootstrapTask3 : MonoBehaviour
{
    [SerializeField] private CoinSpawner _spawner;

    private void Awake()
    {
        _spawner.Initialize();
    }
}

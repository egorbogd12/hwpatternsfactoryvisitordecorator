﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CoinSpawner : MonoBehaviour
{
    [SerializeField] private float _spawnCooldown;
    [SerializeField] private List<Transform> _spawnPointsList;
    [SerializeField] private CoinFactory _coinFactory;

    private SpawnPositions _spawnPositions;
    private Coroutine _spawnCoroutine;
    private bool _spawnPointsFull = false;

    private void Update()
    {
        RestartCoroutine();
    }

    public void Initialize()
    {
        _spawnPositions = new SpawnPositions(_spawnPointsList.Count);

        StartWork();
    }

    [ContextMenu("Start")]
    public void StartWork()
    {
        StopWork();

        _spawnCoroutine = StartCoroutine(Spawn());
    }

    public void StopWork()
    {
        if (_spawnCoroutine != null)
        {
            StopCoroutine(_spawnCoroutine);
        }
    }

    private IEnumerator Spawn()
    {
        while (_spawnPositions.HasEmptyPoint())
        {
            Coin coin = _coinFactory.Get(GetRandomCoinType());

            coin.OnDeleted += OnCoinDeleted;

            coin.MoveToPosition(GetRandomPosition());

            yield return new WaitForSeconds(_spawnCooldown);
        }
    }

    private void OnCoinDeleted(Coin coin)
    {
        coin.OnDeleted -= OnCoinDeleted;
        _spawnPositions.RemovePointFromOccupiedList(coin.transform.position);
    }

    private void RestartCoroutine()
    {
        if (_spawnPositions.HasEmptyPoint() == false)
        {
            _spawnPointsFull = true;
        }

        if (_spawnPositions.HasEmptyPoint() && _spawnPointsFull)
        {
            float timeToRespawn = _spawnCooldown;
            timeToRespawn -= Time.deltaTime;
            if (timeToRespawn <= 0)
            {
                StartWork();
                _spawnPointsFull = false;
            }
        }
    }

    private CoinType GetRandomCoinType()
    {
        return (CoinType) Random.Range(0, Enum.GetValues(typeof(CoinType)).Length);
    }

    private Vector3 GetRandomPosition()
    {
        List<Vector3> occupiedList = _spawnPositions.GetOccupiedList();

        Transform spawnPoint = _spawnPointsList[Random.Range(0, _spawnPointsList.Count)];

        do
        {
            spawnPoint = _spawnPointsList[Random.Range(0, _spawnPointsList.Count)];
        } while (occupiedList.Contains(spawnPoint.position) == true);

        _spawnPositions.AddPointToOccupiedList(spawnPoint.position);
        return spawnPoint.position;
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPositions
{
    private List<Vector3> _occupiedPointsList;
    private int _ammountOfPointsToSpawn;
    
    public SpawnPositions(int ammountOfPoints)
    {
        _ammountOfPointsToSpawn = ammountOfPoints;
        _occupiedPointsList = new List<Vector3>();
    }

    public void AddPointToOccupiedList(Vector3 point)
    {
        if (_occupiedPointsList.Contains(point))
            throw new ArgumentException(nameof(point));

        _occupiedPointsList.Add(point);
    }
    
    public void RemovePointFromOccupiedList(Vector3 point)
    {
        if (_occupiedPointsList.Contains(point))
            _occupiedPointsList.Remove(point);
        
        else
        {
            throw new ArgumentException(nameof(point));
        }
    }

    public List<Vector3> GetOccupiedList()
    {
        return _occupiedPointsList;
    }

    public bool HasEmptyPoint()
    {
        return _occupiedPointsList.Count < _ammountOfPointsToSpawn;
    }
}
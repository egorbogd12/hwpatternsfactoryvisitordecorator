﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "CoinFactory", menuName = "Factories/CoinFactory")]
public class CoinFactory : ScriptableObject
{
    [SerializeField] private CoinConfig _empty, _small, _big;

    public Coin Get(CoinType coinType)
    {
        CoinConfig config = GetCoinConfig(coinType);
        Coin instance = Instantiate(config.CoinPrefab);
        instance.Initialize(config.ValueToAdd);
        return instance;
    }

    private CoinConfig GetCoinConfig(CoinType coinType)
    {
        switch (coinType)
        {
            case CoinType.Big:
                return _big;

            case CoinType.Empty:
                return _empty;

            case CoinType.Small:
                return _small;

            default:
                throw new ArgumentException(nameof(coinType));
        }
    }
}
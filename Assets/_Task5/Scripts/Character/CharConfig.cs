﻿
using System;
using UnityEngine;

[Serializable]
public class CharConfig
{
    [SerializeField] private Character _characterPrefab;
    // [SerializeField, Range(0, 100)] private int _strength;
    // [SerializeField, Range(0, 100)] private int _agility;
    // [SerializeField, Range(0, 150)] private int _maxHealth;
    // [SerializeField, Range(0, 100)] private int _intellect;

    public Character CharacterPrefab => _characterPrefab;
    
    // public int Strength => _strength;
    //
    // public int Agility => _agility;
    //
    // public int MaxHealth => _maxHealth;
    //
    // public int Intellect => _intellect;
}
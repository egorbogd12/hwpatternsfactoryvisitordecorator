using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PassiveAbillityType
{
    MeleeAttack,
    RangeAttack,
    MagicAttack,
}
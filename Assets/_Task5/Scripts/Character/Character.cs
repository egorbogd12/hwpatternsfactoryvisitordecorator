﻿using System;
using UnityEngine;

public abstract class Character: MonoBehaviour
{
    public event Action<Character> Died;

    public void MoveTo(Vector3 position) => transform.position = position;

    public void Die()
    {
        Died?.Invoke(this);
        Destroy(gameObject);
    }
}
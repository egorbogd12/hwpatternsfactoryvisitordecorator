﻿using System;
using Assets.Visitor;
using UnityEngine;

[CreateAssetMenu(fileName = "character", menuName = "factories")]
public class CharFactory: ScriptableObject
{
    [SerializeField] private CharConfig _ork, _elf, _human;

    public Character Get(CharactersType type)
    {
        switch (type)
        {
            case CharactersType.Elf:
                return Instantiate(_elf.CharacterPrefab);

            case CharactersType.Human:
                return Instantiate(_human.CharacterPrefab);

            case CharactersType.Ork:
                return Instantiate(_ork.CharacterPrefab);
            default:
                throw new ArgumentException(nameof(type));
        }
    }
}
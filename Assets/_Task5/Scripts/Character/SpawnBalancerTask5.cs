﻿using Assets.Visitor;
using UnityEngine;

public class SpawnBalancerTask5
{
    public int SpawnedWeight => _enemyVisiter.SpawnedWeight;

    private SpawnerTask5 _spawner;
    private EnemyVisiter _enemyVisiter;
    private int _spawnWeightLimit;

    public SpawnBalancerTask5(SpawnerTask5 spawner, int spawnWeightLimit)
    {
        _spawner = spawner;
        _spawner.OnCharSpawned += OnCharSpawned;
        _enemyVisiter = new EnemyVisiter();
        _spawnWeightLimit = spawnWeightLimit;
    }

    ~SpawnBalancerTask5() => _spawner.OnCharSpawned -= OnCharSpawned;

    public bool CanSpawn()
    {
        return SpawnedWeight < _spawnWeightLimit;
    }


    private void OnCharSpawned(Enemy enemy1)
    {
        //_enemyVisiter.Visit(enemy1);
        Debug.Log($"Балансный вес спавнера: {SpawnedWeight}, максимум : {_spawnWeightLimit}");
    }

    private class EnemyVisiter : IEnemyVisitor
    {
        public int SpawnedWeight { get; private set; }

        public void Visit(Ork ork) => SpawnedWeight += 20;

        public void Visit(Human human) => SpawnedWeight += 5;

        public void Visit(Elf elf) => SpawnedWeight += 10;

        public void Visit(Assets.Visitor.Enemy enemy) => Visit((dynamic) enemy);

        public void Visit(Vampire vampire) => SpawnedWeight += 25;
    }
}
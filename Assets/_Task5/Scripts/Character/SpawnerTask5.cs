﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Visitor;
using UnityEngine;

public class SpawnerTask5 : MonoBehaviour
{
    [SerializeField] private float _spawnCooldown;
    [SerializeField] private List<Transform> _spawnPoints;
    [SerializeField] private CharFactory _сharFactory;

    private List<Character> _spawnedСharacters = new List<Character>();
    private SpawnBalancerTask5 _spawnBalancer;
    private Coroutine _spawn;

    public event Action<Enemy> OnCharSpawned;
    public event Action<Enemy> Notified;

    public void Initialize()
    {
        _spawnBalancer = new SpawnBalancerTask5(this, 100);

        StartWork();
    }

    public void StartWork()
    {
        StopWork();

        _spawn = StartCoroutine(Spawn());
    }

    public void StopWork()
    {
        if (_spawn != null)
            StopCoroutine(_spawn);
    }

    [ContextMenu("Kill")]
    public void KillRandomChar()
    {
        if (_spawnedСharacters.Count < 0)
            return;

        _spawnedСharacters[UnityEngine.Random.Range(0, _spawnedСharacters.Count)].Die();
    }

    private IEnumerator Spawn()
    {
        while (_spawnBalancer.CanSpawn())
        {
            // Character character = GetRandomEnemy();
            //
            // enemy.MoveTo(GetRandomSpawnPoint());
            //
            // OnCharSpawned?.Invoke(enemy);
            // //enemy.Died += OnEnemyDied;
            //
            // _spawnedСharacters.Add(enemy);


            yield return new WaitForSeconds(_spawnCooldown);
        }
    }

    private Vector3 GetRandomSpawnPoint()
    {
        return _spawnPoints[UnityEngine.Random.Range(0, _spawnPoints.Count)].position;
    }

    // private Enemy GetRandomEnemy()
    // {
    //     //  return _сharFactory.Get((EnemyType) UnityEngine.Random.Range(0, Enum.GetValues(typeof(EnemyType)).Length));
    // }

    private void OnEnemyDied(Enemy enemy)
    {
        // enemy.Died -= OnEnemyDied;
        // Notified?.Invoke(enemy);
        // _spawnedEnemies.Remove(enemy);
    }
}
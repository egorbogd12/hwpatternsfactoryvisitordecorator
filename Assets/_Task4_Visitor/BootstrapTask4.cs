﻿using UnityEngine;

namespace Assets.Visitor
{
    public class BootstrapTask4 : MonoBehaviour
    {
        [SerializeField] private Assets.Visitor.Spawner _spawner;

        private Score _score;
        private void Awake()
        {
            _spawner.Initialize();
            _score = new Score(_spawner);
        }
    }
}
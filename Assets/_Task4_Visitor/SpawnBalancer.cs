﻿using Assets.Visitor;
using UnityEngine;

namespace Assets.Visitor
{
    public class SpawnBalancer
    {
        public int SpawnedWeight => _enemyVisiter.SpawnedWeight;

        private Spawner _spawner;
        private EnemyVisiter _enemyVisiter;
        private int _spawnWeightLimit;

        public SpawnBalancer(Spawner spawner, int spawnWeightLimit)
        {
            _spawner = spawner;
            _spawner.OnEnemySpawned += OnEnemySpawned;
            _enemyVisiter = new EnemyVisiter();
            _spawnWeightLimit = spawnWeightLimit;
        }

        ~SpawnBalancer() => _spawner.OnEnemySpawned -= OnEnemySpawned;

        public bool CanSpawn()
        {
            return SpawnedWeight < _spawnWeightLimit;
        }


        private void OnEnemySpawned(Assets.Visitor.Enemy enemy)
        {
            enemy.Accept(_enemyVisiter);
            Debug.Log($"Балансный вес спавнера: {SpawnedWeight}, максимум : {_spawnWeightLimit}");
        }

        private class EnemyVisiter : IEnemyVisitor
        {
            public int SpawnedWeight { get; private set; }

            public void Visit(Ork ork) => SpawnedWeight += 20;

            public void Visit(Human human) => SpawnedWeight += 5;

            public void Visit(Elf elf) => SpawnedWeight += 10;

          //  public void Visit(Assets.Visitor.Enemy enemy) => Visit((dynamic) enemy);

            public void Visit(Vampire vampire) => SpawnedWeight += 25;
        }
    }
}
﻿namespace Assets.Visitor
{
    public class Vampire: Enemy
    {
        public override void Accept(IEnemyVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}

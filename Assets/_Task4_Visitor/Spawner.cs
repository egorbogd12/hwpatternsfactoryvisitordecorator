﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Visitor
{
    public class Spawner : MonoBehaviour, IEnemyDeathNotifier
    {
        [SerializeField] private float _spawnCooldown;
        [SerializeField] private List<Transform> _spawnPoints;
        [SerializeField] private EnemyFactory _enemyFactory;

        private List<Enemy> _spawnedEnemies = new List<Enemy>();
        
        private SpawnBalancer _spawnBalancer;
        private Coroutine _spawn;

        public event Action<Enemy> OnEnemySpawned;
        public event Action<Enemy> Notified;

        public void Initialize()
        {
            _spawnBalancer = new SpawnBalancer(this, 100);

            StartWork();
        }

        public void StartWork()
        {
            StopWork();

            _spawn = StartCoroutine(Spawn());
        }

        public void StopWork()
        {
            if (_spawn != null)
                StopCoroutine(_spawn);
        }

        [ContextMenu("Kill")]
        public void KillRandomEnemy()
        {
            if (_spawnedEnemies.Count < 0)
                return;

            _spawnedEnemies[UnityEngine.Random.Range(0, _spawnedEnemies.Count)].Die();
        }

        private IEnumerator Spawn()
        {
            while (_spawnBalancer.CanSpawn())
            {
                Enemy enemy = GetRandomEnemy();

                enemy.MoveTo(GetRandomSpawnPoint());

                OnEnemySpawned?.Invoke(enemy);
                enemy.Died += OnEnemyDied;

                _spawnedEnemies.Add(enemy);


                yield return new WaitForSeconds(_spawnCooldown);
            }
        }

        private Vector3 GetRandomSpawnPoint()
        {
            return _spawnPoints[UnityEngine.Random.Range(0, _spawnPoints.Count)].position;
        }

        private Enemy GetRandomEnemy()
        {
            return _enemyFactory.Get((EnemyType) UnityEngine.Random.Range(0, Enum.GetValues(typeof(EnemyType)).Length));
        }

        private void OnEnemyDied(Enemy enemy)
        {
            enemy.Died -= OnEnemyDied;
            Notified?.Invoke(enemy);
            _spawnedEnemies.Remove(enemy);
        }
    }
}